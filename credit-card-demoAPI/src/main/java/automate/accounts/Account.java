package automate.accounts;

import automate.currency.CurrencyAmount;
import automate.transactions.MonetaryTransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class Account {
    // todo: should probably figure out some scheme to keep this ID immutable.
    private UUID id;
    private String owner;
    private String name;
    private boolean open;
    private CurrencyAmount balance;

    private List<MonetaryTransaction> transactions;

    Account () {
        id = UUID.randomUUID();
        owner = "";
        name = "";
        open = true;
        balance = new CurrencyAmount();
        transactions = new ArrayList<>();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public CurrencyAmount getBalance() {
        return balance;
    }

    public void setBalance(CurrencyAmount balance) {
        this.balance = balance;
    }

    public List<MonetaryTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<MonetaryTransaction> transactions) {
        this.transactions = transactions;
    }
}
