package automate.accounts;

class AccountIdNotFoundException extends Exception {
    AccountIdNotFoundException (String message) {
        super(message);
    }
}
