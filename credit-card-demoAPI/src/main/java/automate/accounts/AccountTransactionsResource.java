package automate.accounts;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/transactions/{transactionId}")
@DenyAll
@Produces(MediaType.APPLICATION_JSON)
public class AccountTransactionsResource {

    private AccountsService accountsService;

    public AccountTransactionsResource() {
        // public noargs constructor for jaxrs
    }

    @Inject
    public AccountTransactionsResource(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @GET
    @PermitAll
    public Response getTransaction(@PathParam("transactionId") UUID transactionId) {
        return Response.ok().build();
    }

    private Response handleAccountNotFoundException(AccountIdNotFoundException accountNotFoundException) {
        return Response.serverError()
                .status(Response.Status.NOT_FOUND)
                .entity(accountNotFoundException.getMessage())
                .build();
    }
}
