package automate.accounts;

import automate.transactions.MonetaryTransaction;
import io.swagger.annotations.Api;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

@Path("/accounts")
@DenyAll
@Api(value = "/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountsResource {

    private AccountsService accountsService;

    public AccountsResource() {
        // public no-args constructor for jaxrs
    }

    @Inject
    public AccountsResource(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @GET
    @PermitAll
    public Response getAccounts(@DefaultValue("true") @QueryParam("open") boolean openAccounts) {
        // this probably won't scale well if additional query parameters are used.
        // consider building a query parameters object to pass through to the service.
        Response response;
        List<Account> filteredAccounts;

        if (openAccounts) {
            filteredAccounts = accountsService.getOpenAccounts();
        } else {
            filteredAccounts = accountsService.getClosedAccounts();
        }

        response = Response.ok().entity(filteredAccounts).build();

        return response;
    }

    @GET
    @PermitAll
    @Path("/{accountId}")
    // todo: consider extracting all /{accountId} resources into a new resource class.
    // all of the 'getAccount' code could be extracted into the constructor!
    public Response getAccount(@PathParam("accountId") UUID id) {
        Response response;
        try {
            Account account = accountsService.getAccount(id);
            response = Response.ok().entity(account).build();
        } catch (AccountIdNotFoundException accountNotFoundException) {
            response = handleAccountNotFoundException(accountNotFoundException);
        }

        return response;
    }

    @POST
    @PermitAll
    public Response createNewAccount(Account accountToCreate, @Context UriInfo uriInfo) {
        Response response;
        Account createdAccount = null;
        try {
            createdAccount = accountsService.openAccount(accountToCreate);

            URI createdAccountUri = new URI(uriInfo.getRequestUri() + "/" + createdAccount.getId());
            response = Response.created(createdAccountUri).entity(createdAccount).build();
        } catch (URISyntaxException uriException) {
            // try to clean up after ourselves if uri creation fails.
            // so if uri creation fails, we still create this entry in the underlying service, we just close it.
            if (createdAccount != null && createdAccount.getId() != null) {
                response = closeAccount(createdAccount.getId());
            } else {
                response = Response.serverError()
                        .entity("Could not provide a link to the newly opened account.")
                        .build();
            }
        }

        return response;
    }

    @DELETE
    @PermitAll
    @Path("/{accountId}")
    public Response closeAccount(@PathParam("accountId") UUID accountId) {
        Response response;

        try {
            Account account;

            accountsService.closeAccount(accountId);

            account = accountsService.getAccount(accountId);
            response = Response.ok().entity(account).build();
        } catch (AccountIdNotFoundException accountNotFoundException) {
            response = handleAccountNotFoundException(accountNotFoundException);
        }

        return response;
    }

    @GET
    @PermitAll
    @Path("/{accountId}/transactions")
    public Response getRelatedTransactions(@PathParam("accountId") UUID accountId) {
        Response response;
        List<MonetaryTransaction> transactions;

        try {
            Account account = accountsService.getAccount(accountId);
            transactions = account.getTransactions();

            response = Response.ok().entity(transactions).build();
        } catch (AccountIdNotFoundException accountNotFoundException) {
            response = handleAccountNotFoundException(accountNotFoundException);
        }

        return response;
    }

    @POST
    @PermitAll
    @Path("/{accountId}/transactions")
    public Response addTransaction (@PathParam("accountId") UUID accountId, MonetaryTransaction transaction, @Context UriInfo uriInfo) {
        Response response;

        try {
            Account account = accountsService.getAccount(accountId);

            transaction.setId(UUID.randomUUID());

            account.getTransactions().add(transaction); //todo: this is a big 'ol method chain. smelly code!

            URI createdTransactionUri = new URI(uriInfo.getRequestUri() + "/" + transaction.getId());
            response = Response.created(createdTransactionUri).entity(transaction).build();
        } catch (URISyntaxException urise) {
            response = Response.serverError().entity("Failed to construct transaction url for transaction with ID: " + transaction.getId()).build();
        } catch (AccountIdNotFoundException accountNotFoundException) {
            response = handleAccountNotFoundException(accountNotFoundException);
        }

        return response;
    }

    private Response handleAccountNotFoundException(AccountIdNotFoundException accountNotFoundException) {
        return Response.serverError()
                .status(Response.Status.NOT_FOUND)
                .entity(accountNotFoundException.getMessage())
                .build();
    }
}
