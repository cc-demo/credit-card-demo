package automate.accounts;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class AccountsService {

    private HashMap<UUID, Account> accounts;

    public AccountsService() {
        accounts = new HashMap<>();
    }

    List<Account> getOpenAccounts() {
        return accounts.values()
                .stream()
                .filter(Account::isOpen)
                .collect(Collectors.toList());
    }

    List<Account> getClosedAccounts() {
        return accounts.values()
                .stream()
                .filter(account -> !account.isOpen())
                .collect(Collectors.toList());
    }

    Account getAccount(UUID accountId) throws AccountIdNotFoundException {
        Account account;

        if (accounts.containsKey(accountId)) {
            account = accounts.get(accountId);
        } else {
            throw new AccountIdNotFoundException ("Could not find account with id:" + accountId);
        }

        return account;
    }

    Account closeAccount(UUID accountId) throws AccountIdNotFoundException {
        Account closedAccount = getAccount(accountId);
        closedAccount.setOpen(false);

        return closedAccount;
    }

    Account openAccount(Account account) {
        UUID accountId = UUID.randomUUID();
        account.setId(accountId);

        account.setOpen(true);


        accounts.put(accountId, account);


        return account;
    }
}
