package automate.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import automate.accounts.AccountsResource;

@ApplicationPath("")
public class CreditCardDemoApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new HashSet<>();

		resources.add(AccountsResource.class);
		
		return resources;
	}
}
