package automate.currency;

import java.math.BigDecimal;

public class CurrencyAmount {
    private CurrencyCode currency;
    private BigDecimal decimal;
    private int integer;
    private int integerScale;

    public CurrencyAmount () {
        currency = CurrencyCode.USD;
        decimal = new BigDecimal(0);
        integer = 0;
        integerScale = 0;
    }

    public CurrencyCode getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyCode currency) {
        this.currency = currency;
    }

    public BigDecimal getDecimal() {
        return decimal;
    }

    public void setDecimal(BigDecimal decimal) {
        this.decimal = decimal;
    }

    public int getInteger() {
        return integer;
    }

    public void setInteger(int integer) {
        this.integer = integer;
    }

    public int getIntegerScale() {
        return integerScale;
    }

    public void setIntegerScale(int integerScale) {
        this.integerScale = integerScale;
    }
}
