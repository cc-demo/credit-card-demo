package automate.transactions;

import automate.currency.CurrencyAmount;

import java.time.LocalDateTime;
import java.util.UUID;

public class MonetaryTransaction {
    private UUID id;
    private LocalDateTime date;
    private CurrencyAmount amount;
    private String description;

    public MonetaryTransaction () {
        description = "";
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public CurrencyAmount getAmount() {
        return amount;
    }

    public void setAmount(CurrencyAmount amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
